__Проект базируется на микро сервисной архектируй Spring Cloud, состоит из 8 микросервисов.__
1. config отвечает за хранения и предоставления свойства для других сервисов
---
2. registration регистрирует сервис (нужен для gateway)
---
3. gateway отвечает за перенаправления http запрос к сервисам(для нахождения сервиса обращается к registration)
---
4. initialization-database отвечает за создание таблиц в БД
---
5. notification принимает сообщения от rabbitmq для отправки сообщений
---
6. driver отвечает за операции связанные с driver
   [readme.md](https://gitlab.com/taxi10/driver/-/blob/main/readme.md)
---
7. company отвечает за операции связанные с company
   [readme.md](https://gitlab.com/taxi10/company/-/blob/main/readme.md)
---
8. order отвечает за операции связанные с order
   [readme.md](https://gitlab.com/taxi10/order/-/blob/main/readme.md)
---
9. в docker поднята БД postgresql
---
10. в docker поднят rabbitmq 

Проект не имеет frontend части, для обращения к backend используем postman.
Все запросы посылаются на url http://130.193.53.230:8989/ `service`

Форма авторизации  http://130.193.53.230:8989/login нужна для получение session.
